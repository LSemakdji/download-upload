<?php

namespace App\Controller;

use App\Entity\Fichier;
use App\Form\FichierType;
use App\Repository\FichierRepository;
use SebastianBergmann\Type\Exception;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class FichierController extends AbstractController
{
    #[Route('/', name: 'homepage')]
    public function index(
        Request $request,
        EntityManagerInterface $manager,
        FichierRepository $fichierRepository
    ): Response {
        $fichier = new Fichier();
        $form = $this->createForm(FichierType::class, $fichier);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // on recupere les infos du champ imageFile
            /** @var UploadedFile $imageFile */
            $imageFile = $form->get('imageFile')->getData();
            if ($imageFile) {
                $originalFilename = pathinfo($imageFile->getClientOriginalName(), PATHINFO_FILENAME);
                $newFilename =  $originalFilename . '-' . uniqid() . '.' . $imageFile->guessExtension();
                try {
                    $imageFile->move(
                        $this->getParameter('cv'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }
                $fichier->setFile($newFilename);
            }

            $manager->persist($fichier);
            $manager->flush();
            return $this->redirectToRoute('homepage');
        }

        return $this->render('fichier/index.html.twig', [
            'form' => $form->createView(),
            'fichiers' => $fichierRepository->findAll(),
        ]);
    }
    #[Route('/download/{id}', name: 'download')]
    public function downloadAction($id, FichierRepository $fichierRepository)
    {
        try {
            $fichier = $fichierRepository->find($id);
            if (!$fichier) {
                $array = array(
                    'status' => 0,
                    'message' => 'File does not exist'
                );
                $response = new JsonResponse($array, 200);
                return $response;
            }

            $fileName = $fichier->getFile();
            $file_with_path = $this->getParameter('cv') . "/" . $fileName;
            $response = new BinaryFileResponse($file_with_path);
            $response->headers->set('Content-Type', 'text/plain');

            return $response;
        } catch (Exception $e) {
            $array = array(
                'status' => 0,
                'message' => 'Download error'
            );
            $response = new JsonResponse($array, 400);
            return $response;
        }
    }
}
